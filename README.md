## Backend Web

```bash
$cd backend-web

$npm install
```
Edit File .env
```env
DB_HOST=localhost
DB_USER=root
DB_PASS=
DB_NAME=auto_assess_web_rev


AUTO_ASSESS_BACKEND=http://localhost:8081
PORT=8080
```
Menjalankan Seeder
```bash
$npm run seed
```

Menjalankan server
```bash
$npm start
```

## Backend Assessment
```bash
$cd backend-assessment

$npm install
```
Edit File .env
```env
DB_HOST=localhost
DB_USER=root
DB_PASS=

PORT=8080
```

Menjalankan server
```bash
$npm start
```

## Frontend
```bash
$cd frontend-web
```

Install Packages (Express Server dan Client)
```bash
$npm run install-packages
```

Config Frontend di client/src/config/index.js
```javascript
export const ENDPOINT = "http://localhost:8080"
...

export const ADMIN_CREDENTIAL = {
    username: "admin",
    password: "12345"
}
```

Build client
```bash
$npm run client-build
```

Menjalankan Server
```bash
$npm start
```
---

## LAIN - LAIN
### Konfigurasi Nilai (Manual), di backend-web/config/gradingRules.js
```javascript
function gradingRulesLatihan(attempt) {
    if (attempt <= 3) {
        return 100
    }
    if (attempt <= 6) {
        return 95
    }
    if (attempt <= 9) {
        return 90
    }
    if (attempt <= 12) {
        return 80
    }
    if (attempt <= 15) {
        return 70
    }
    if (attempt <= 18) {
        return 60
    }
    return 50
}

function gradingRulesUjian(attempt) {
    if (attempt <= 2) {
        return 100
    }
    if (attempt <= 3) {
        return 95
    }
    if (attempt <= 4) {
        return 90
    }
    if (attempt <= 5) {
        return 85
    }
    if (attempt <= 6) {
        return 80
    }
    if (attempt <= 7) {
        return 75
    }
    if (attempt <= 8) {
        return 70
    }
    if (attempt <= 9) {
        return 65
    }
    if (attempt <= 10) {
        return 60
    }
    if (attempt <= 11) {
        return 55
    }
    return 50
}
...
```

### Username dan Password User (Dosen) & Mahasiswa
- Untuk Password, secara default akan sama dengan username
- Untuk Mahasiswa, secara default NIM akan menjadi username, sekaligus menjadi password
- Dosen:
    ```
    Username: dosencoba
    Password: dosencoba
    ```
- Mahasiswa:
    ```
    Username: 1741710101
    Password: 1741710101
    ```