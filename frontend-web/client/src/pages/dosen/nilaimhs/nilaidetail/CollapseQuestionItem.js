import { faArrowCircleDown, faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Card, CardBody, CardHeader, Col, Collapse, Row } from 'reactstrap'

function CollapseQuestionItem({ index, toggle, collapse, data }) {
    const answers = JSON.parse(data.questionKey)
    return (
        <Card style={{ marginBottom: '1rem' }}>
            <CardHeader>
                <Row onClick={toggle} data-event={index} className="align-items-center">
                    <Col md={9} onClick={toggle} data-event={index}>
                        <span onClick={toggle} data-event={index} className="mb-0" dangerouslySetInnerHTML={{ __html: data.question }}></span>
                    </Col>
                    <Col onClick={toggle} data-event={index} className="text-right">
                        <span onClick={toggle} data-event={index}>Jumlah Attempt: {data.answer.length} <FontAwesomeIcon onClick={toggle} data-event={index} className="ml-2 cursor-pointer" icon={faArrowCircleDown} /></span>
                    </Col>
                </Row>
            </CardHeader>
            <Collapse isOpen={collapse === index}>
                <CardBody>
                    <div className="text-muted">
                        <h6>Kunci Jawaban: </h6>
                        <ul>
                            {
                                answers.map((answer, index) => {
                                    return (
                                        <li key={index}>{answer}</li>
                                    )
                                })
                            }
                        </ul>
                        {/* {data.questionKey} */}
                    </div>
                    <hr />
                    {data.answer.map((val, index) => {
                        return (
                            <div className="mb-4" key={index}>
                                <Row>
                                    <Col md={6}>
                                        <h6>Tipe: <span className={val.type === "test" ? "text-warning" : "text-success"}>{val.type.toUpperCase()}</span> </h6>
                                        {val.text}
                                    </Col>
                                    <Col>
                                        <h2 className={(val.isEqual) ? "text-success" : "text-danger"}>
                                            <FontAwesomeIcon icon={(val.isEqual) ? faCheckCircle : faTimesCircle} />
                                        </h2>
                                    </Col>
                                </Row>

                                <hr />
                            </div>
                        )
                    })}
                </CardBody>
            </Collapse>
        </Card>
    )
}

export default CollapseQuestionItem
