import React from "react";
import classNames from "classnames";
import { Container } from "reactstrap";
import { Switch, Route } from "react-router-dom";

import TopbarAdmin from "./TopbarAdmin";
import { adminRoutes } from "../../../routes";

const ContentAdmin = ({ sidebarIsOpen, toggleSidebar }) => {
    function renderRoutes() {
        return adminRoutes.map((route, index) => (
            <Route
                path={route.path}
                component={route.component}
                key={index}
                exact
            />
        ));
    }

    return (
        <Container
            fluid
            className={classNames("content", { "is-open": sidebarIsOpen })}
        >
            <TopbarAdmin toggleSidebar={toggleSidebar} />
            <Switch>
                {/* <Route exact path="/" component={() => "Hello"} /> */}
                {renderRoutes()}
            </Switch>
        </Container>
    );
};

export default ContentAdmin;
