import React, { useState } from "react";
import ContentAdmin from "../contentadmin/ContentAdmin";
import SideBarAdmin from "../sidebaradmin/";

function DashboardAdmin(props) {
    const [sidebarIsOpen, setSidebarOpen] = useState(true);
    const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);

    return (
        <>
            <SideBarAdmin toggle={toggleSidebar} isOpen={sidebarIsOpen} />
            <ContentAdmin
                toggleSidebar={toggleSidebar}
                sidebarIsOpen={sidebarIsOpen}
            />
        </>
    );
}

export default DashboardAdmin;
