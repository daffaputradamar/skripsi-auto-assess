import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, FormGroup, Input, Label, Row, Spinner } from 'reactstrap'
import DataTable from '../../../components/datatable'
import { ENDPOINT_BACKEND } from '../../../config'

const INITIAL_STATE = {
    name: "",
    no_induk: "",
    username: "",
}
function Dosen(props) {
    const adminIsLoggedIn = localStorage.getItem("admin") || null
    if (!adminIsLoggedIn) {
        props.history.push('/admin')
    }

    const [isFormShown, setIsFormShown] = useState(false)
    const [dosenForm, setDosenForm] = useState(INITIAL_STATE)
    const [loading, setLoading] = useState(true)
    const [dosen, setDosen] = useState()

    useEffect(() => {
        setLoading(true)
        getDosen()
            .then(() => {
                getDosen()
                    .then(() => {
                        setLoading(false)
                    })
            })
    }, [])

    const getDosen = () => {
        return new Promise((resolve, reject) => {
            axios.get(`${ENDPOINT_BACKEND}/users`)
                .then(({ data }) => {
                    setDosen(data.data)
                    setDatatable({
                        ...datatable,
                        rows: data.data.map(dosen => {
                            return {
                                name: dosen.name,
                                no_induk: dosen.no_induk,
                                username: dosen.username
                            }
                        })
                    })
                    resolve(true)
                })
                .catch(err => reject(false))
        })
    }

    const [datatable, setDatatable] = useState({
        columns: [
            {
                label: "Nama",
                field: "name",
                width: 300,
                attributes: {
                    "aria-controls": "DataTable",
                    "aria-label": "Nama",
                },
            },
            {
                label: "No Induk",
                field: "no_induk",
            },
            {
                label: "Username",
                field: "username",
            },
        ],
        rows: [
            {
                name: "Dosen 1",
                no_induk: "1741720017",
                username: "dosen1"
            },
            {
                name: "Dosen 2",
                no_induk: "1741720017",
                username: "dosen2"
            },
        ],
    });

    const submitForm = (e) => {
        e.preventDefault()
        const data = { ...dosenForm, password: dosenForm.username }
        axios.post(`${ENDPOINT_BACKEND}/users`, data)
            .then(() => {
                setLoading(true)
                getDosen()
                    .then(() => {
                        setIsFormShown(false)
                        setDosenForm(INITIAL_STATE)
                        setLoading(false)
                    })
            })
    }

    return (
        <Container>
            {loading ? <Spinner /> : <div>
                <h5 className="mb-4">List Dosen</h5>
                <div className="text-right mt-5">
                    <Button color="info" outline onClick={e => setIsFormShown(!isFormShown)}>Tambah Dosen</Button>
                </div>
                {isFormShown && (<div className="my-4">
                    <Row>
                        <Col md={6} sm={12}>
                            <Form onSubmit={submitForm}>
                                <FormGroup>
                                    <Label for="no_induk">Nomor Induk</Label>
                                    <Input type="text" id="no_induk" name="no_induk" value={dosenForm.no_induk} onChange={e => setDosenForm({ ...dosenForm, [e.target.name]: e.target.value })} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="name">Nama</Label>
                                    <Input type="text" id="name" name="name" value={dosenForm.name} onChange={e => setDosenForm({ ...dosenForm, [e.target.name]: e.target.value })} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="username">Username</Label>
                                    <Input type="text" id="username" name="username" value={dosenForm.username} onChange={e => setDosenForm({ ...dosenForm, [e.target.name]: e.target.value })} />
                                </FormGroup>
                                <div className="text-right">
                                    <Button type="submit" className="purple-button">Simpan</Button>
                                </div>
                            </Form>
                        </Col>
                        <Col md={6}></Col>
                    </Row>
                </div>)}
                <DataTable datatable={datatable} />
            </div>}
        </Container>
    )
}

export default Dosen
