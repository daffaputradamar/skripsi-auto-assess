import { faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext, useState } from "react";
import { Alert, Button, Card, CardBody, Col, Container, Form, FormGroup, Input, Label } from "reactstrap";
import { ADMIN_CREDENTIAL } from "../../../config";

const INITIAL_STATE = {
    username: "",
    password: "",
};

function LoginAdmin(props) {
    const adminIsLoggedIn = localStorage.getItem("admin") || null

    const [loginForm, setLoginForm] = useState(INITIAL_STATE);

    const [isSuccess, setIsSuccess] = useState(true)
    const [errMessage, setErrMessage] = useState("")

    const redirectIfAuthenticated = (isLoggedIn) => {
        if (isLoggedIn) {
            props.history.push('/admin/dosen')
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        const adminCred = ADMIN_CREDENTIAL
        if (loginForm.username !== adminCred.username) {
            setIsSuccess(false)
            setErrMessage("Username tidak dikenali")
            setLoginForm(INITIAL_STATE);
            return;
        }

        if (loginForm.password !== adminCred.password) {
            setIsSuccess(false)
            setErrMessage("Password salah")
            setLoginForm({ ...loginForm, password: "" });
            return;
        }

        localStorage.setItem("admin", true)
        props.history.push('/admin/dosen')

    };

    return (
        <Container>
            {
                redirectIfAuthenticated(adminIsLoggedIn)
            }
            <div className="vh-100 d-flex justify-content-center align-items-center">
                <Card className="w-50">
                    <CardBody>
                        <div className="my-3">
                            <h1 className="text-center mb-3">
                                <FontAwesomeIcon
                                    icon={faLock}
                                    className="mr-2"
                                    color="#244282"
                                />
                            </h1>
                        </div>
                        {!isSuccess && <Alert color="danger">
                            {errMessage}
                        </Alert>}
                        <Form onSubmit={onSubmit}>
                            <Col>
                                <FormGroup>
                                    <Label for="username">Username</Label>
                                    <Input
                                        required
                                        type="text"
                                        name="username"
                                        id="username"
                                        placeholder="Username"
                                        value={loginForm.username}
                                        onChange={(e) =>
                                            setLoginForm({
                                                ...loginForm,
                                                username: e.target.value,
                                            })
                                        }
                                    />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="password">Password</Label>
                                    <Input
                                        required
                                        type="password"
                                        name="password"
                                        id="password"
                                        placeholder="********"
                                        value={loginForm.password}
                                        onChange={(e) =>
                                            setLoginForm({
                                                ...loginForm,
                                                password: e.target.value,
                                            })
                                        }
                                    />
                                </FormGroup>
                            </Col>
                            <div className="text-center mt-4">
                                <Button size="lg" className="purple-button">
                                    Login
                                </Button>
                            </div>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        </Container>
    )
}

export default LoginAdmin;
