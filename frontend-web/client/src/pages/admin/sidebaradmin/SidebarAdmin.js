import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavItem, NavLink, Nav, Row, Col } from "reactstrap";
import classNames from "classnames";
import { Link } from "react-router-dom";
import logoJti from "../../../assets/images/logo_jti.png";

// import SubMenu from "./SubMenu";
import { adminRoutes } from "../../../routes";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
// import { faCopy, faImage, faPaperPlane, faQuestion } from "@fortawesome/free-solid-svg-icons";

const SideBarAdmin = ({ isOpen, toggle }) => {

    return (
        <div className={classNames("sidebar", { "is-open": isOpen })}>
            <div className="sidebar-header">
                <span color="info" onClick={toggle} style={{ color: "#fff" }}>
                    &times;
                </span>
                {/* <h3>Bootstrap Sidebar</h3> */}
                <img src={logoJti} alt="logo jti" className="img-fluid p-3" />
            </div>
            <div className="side-menu">
                <Nav vertical className="list-unstyled py-3">
                    <p className="text-center">Automated Assessment MySQL</p>
                    {adminRoutes.map((route, index) =>
                        route.visible
                        && <NavItem key={index}>
                            <NavLink tag={Link} to={route.path}>
                                <Row>
                                    <Col md="2">
                                        <FontAwesomeIcon
                                            icon={route.icon}
                                            className="mr-2"
                                        />
                                    </Col>
                                    <Col>
                                        {route.name}
                                    </Col>
                                </Row>
                            </NavLink>
                        </NavItem>
                    )}
                    {/* <NavItem>
                        <NavLink tag={Link} to={"/admin/dosen"}>
                            <Row>
                                <Col md="2">
                                    <FontAwesomeIcon
                                        icon={faCalendarAlt}
                                        className="mr-2"
                                    />
                                </Col>
                                <Col>
                                    Dosen
                                </Col>
                            </Row>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={Link} to={"/admin/setting"}>
                            <Row>
                                <Col md="2">
                                    <FontAwesomeIcon
                                        icon={faCalendarAlt}
                                        className="mr-2"
                                    />
                                </Col>
                                <Col>
                                    Setting
                                </Col>
                            </Row>
                        </NavLink>
                    </NavItem> */}
                </Nav>
            </div>
        </div>
    )
};

export default SideBarAdmin;
