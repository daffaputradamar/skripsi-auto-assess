import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Col, Container, Form, FormGroup, Input, Label, Row, Spinner, Button } from 'reactstrap'
import { ENDPOINT_BACKEND } from '../../../config'


function Setting(props) {
    const adminIsLoggedIn = localStorage.getItem("admin") || null
    if (!adminIsLoggedIn) {
        props.history.push('/admin')
    }

    const [loading, setLoading] = useState(false)

    const [threshold, setThreshold] = useState()
    const [thresholdInput, setThresholdInput] = useState(0)
    const [isEditingThreshold, setIsEditingThreshold] = useState(false)

    useEffect(() => {
        setLoading(true)
        getThreshold()
            .then(threshold => {
                setThreshold(threshold)
                setThresholdInput(threshold)
                setLoading(false)
            })
    }, [])

    const getThreshold = () => {
        return new Promise((resolve, reject) => {
            axios.get(`${ENDPOINT_BACKEND}/settings/threshold`)
                .then(({ data }) => {
                    resolve(data)
                })
                .catch(err => reject(false))
        })
    }

    const submitFormThreshold = (e) => {
        e.preventDefault()
        setLoading(true)
        axios.put(`${ENDPOINT_BACKEND}/settings/threshold`, { threshold: thresholdInput })
            .then(() => {
                getThreshold()
                    .then(threshold => {
                        setThreshold(threshold)
                        setThresholdInput(threshold)
                        setIsEditingThreshold(false)
                        setLoading(false)
                    })
            })
    }

    return (
        <Container>
            {loading ? <Spinner /> : <div>
                <h5 className="mb-4">Setting</h5>
                <Form onSubmit={submitFormThreshold}>
                    <Row className="align-items-center">
                        <Col md={6} sm={12}>
                            <FormGroup>
                                <Label for="threshold">Threshold</Label>
                                <Input type="number" id="threshold" name="threshold" min="0.1" step=".01" value={(!isEditingThreshold) ? threshold : thresholdInput} onChange={e => setThresholdInput(e.target.value)} disabled={!isEditingThreshold} />
                            </FormGroup>
                        </Col>
                        <Col md={6} sm={12}>
                            {
                                (!isEditingThreshold) ? (<Button outline onClick={e => setIsEditingThreshold(true)}>
                                    Edit
                                </Button>) : (
                                    <div>
                                        <Button className="purple-button mr-3" type="submit">
                                            Simpan
                                        </Button>
                                        <Button outline onClick={e => setIsEditingThreshold(false)}>
                                            Cancel
                                        </Button>
                                    </div>
                                )
                            }
                        </Col>
                    </Row>
                </Form>
            </div>
            }
        </Container>
    )
}

export default Setting