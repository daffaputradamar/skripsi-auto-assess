export const ENDPOINT = "http://localhost:8080"
export const ENDPOINT_BACKEND = `${ENDPOINT}/api`
export const ENDPOINT_ASSETS = `${ENDPOINT}/images`
export const BEARER_TOKEN = {
    'Authorization': "Bearer " + localStorage.getItem("authToken") || ""
}

export const ADMIN_CREDENTIAL = {
    username: "admin",
    password: "12345"
}