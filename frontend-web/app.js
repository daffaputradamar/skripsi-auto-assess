const express = require('express');
const app = express();
const dotenv = require('dotenv');
const path = require('path');
dotenv.config()

const port = process.env.PORT;

app.use(express.static("client/build"));
app.get('*', function (req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, 'client/build/') });
});

app.listen(port, () => console.log(`Listening on port ${port}`));